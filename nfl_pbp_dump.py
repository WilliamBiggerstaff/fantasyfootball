import nflgame
import csv

#build the sets of key fields to retrieve and store
event_keys = [
"defense_ast",
"defense_ffum",
"defense_fgblk",
"defense_frec",
"defense_frec_tds",
"defense_frec_yds",
"defense_int",
"defense_int_tds",
"defense_int_yds",
"defense_pass_def",
"defense_qbhit",
"defense_safe",
"defense_sk",
"defense_sk_yds",
"defense_tds",
"defense_tkl",
"defense_tkl_loss",
"defense_tkl_loss_yds",
"defense_tkl_primary",
"defense_xpblk",
"first_down",
"fourth_down_att",
"fourth_down_conv",
"fourth_down_failed",
"fumbles_forced",
"fumbles_lost",
"fumbles_notforced",
"fumbles_oob",
"fumbles_rec",
"fumbles_rec_yds",
"fumbles_tot",
"kicking_all_yds",
"kicking_fga",
"kicking_fgb",
"kicking_fgm",
"kicking_fgm_yds",
"kicking_fgmissed",
"kicking_fgmissed_yds",
"kicking_i20",
"kicking_tot",
"kicking_touchback",
"kicking_xpa",
"kicking_xpb",
"kicking_xpmade",
"kicking_xpmissed",
"kicking_yds",
"kickret_ret",
"kickret_touchback",
"kickret_yds",
"passing_att",
"passing_cmp",
"passing_cmp_air_yds",
"passing_first_down",
"passing_incmp",
"passing_incmp_air_yds",
"passing_int",
"passing_sk",
"passing_sk_yds",
"passing_tds"
"passing_twopta",
"passing_twoptm",
"passing_twoptmissed",
"passing_yds",
"penalty",
"penalty_first_down",
"penalty_yds",
"playerid",
"playername",
"punting_i20",
"punting_tot",
"punting_touchback",
"punting_yds",
"puntret_downed",
"puntret_fair",
"puntret_oob",
"puntret_tot",
"puntret_touchback",
"puntret_yds",
"receiving_rec",
"receiving_tar",
"receiving_tds",
"receiving_twopta",
"receiving_twoptm",
"receiving_twoptmissed",
"receiving_yac_yds",
"receiving_yds",
"rushing_att",
"rushing_first_down",
"rushing_loss",
"rushing_loss_yds",
"rushing_tds",
"rushing_yds",
"team",
"third_down_att",
"third_down_conv",
"third_down_failed",
"timeout",
]

play_keys = [
"defense_ast",
"defense_ffum",
"defense_fgblk",
"defense_frec",
"defense_frec_tds",
"defense_frec_yds",
"defense_int",
"defense_int_tds",
"defense_int_yds",
"defense_pass_def",
"defense_qbhit",
"defense_safe",
"defense_sk",
"defense_sk_yds",
"defense_tds",
"defense_tkl",
"defense_tkl_loss",
"defense_tkl_loss_yds",
"defense_tkl_primary",
"defense_xpblk",
"desc",
"down",
"drive",
"first_down",
"formatted_stats",
"fourth_down_att",
"fourth_down_conv",
"fourth_down_failed",
"fumbles_forced",
"fumbles_lost",
"fumbles_notforced",
"fumbles_oob",
"fumbles_rec",
"fumbles_rec_yds",
"fumbles_tot",
"guess_position",
"has_cat",
"has_player",
"home",
"kicking_all_yds",
"kicking_fga",
"kicking_fgb",
"kicking_fgm",
"kicking_fgm_yds",
"kicking_fgmissed",
"kicking_fgmissed_yds",
"kicking_i20",
"kicking_tot",
"kicking_touchback",
"kicking_xpa"
"kicking_xpb",
"kicking_xpmade",
"kicking_xpmissed",
"kicking_yds",
"kickret_ret",
"kickret_touchback",
"kickret_yds",
"name",
"note",
"passer_rating",
"passing_att",
"passing_cmp",
"passing_cmp_air_yds",
"passing_first_down",
"passing_incmp",
"passing_incmp_air_yds",
"passing_int",
"passing_sk",
"passing_sk_yds",
"passing_tds",
"passing_twopta",
"passing_twoptm",
"passing_twoptmissed",
"passing_yds",
"penalty",
"penalty",
"penalty_first_down",
"penalty_yds",
"player",
"playerid",
"players",
"playid",
"punting_i20",
"punting_tot",
"punting_touchback",
"punting_yds",
"puntret_downed",
"puntret_fair",
"puntret_oob",
"puntret_tot",
"puntret_touchback",
"puntret_yds",
"receiving_rec",
"receiving_tar",
"receiving_tds",
"receiving_twopta",
"receiving_twoptm",
"receiving_twoptmissed",
"receiving_yac_yds",
"receiving_yds",
"rushing_att",
"rushing_first_down",
"rushing_loss",
"rushing_loss_yds",
"rushing_tds",
"rushing_yds",
"stats",
"tds",
"team",
"third_down_att",
"third_down_conv",
"third_down_failed",
"time",
"timeout",
"touchdown",
"twopta",
"twoptm",
"twoptmissed",
"yardline",
"yards_togo",
]

player_keys = [
"birthdate",
"college",
"first_name",
"full_name",
"gsis_id",
"gsis_name"
"height",
"last_name",
"name",
"number",
"player_id",
"playerid",
"position",
"profile_id",
"profile_url",
"status",
"team",
"uniform_number",
"weight",
"years_pro",
]

other_keys = [
"play_yardline",
"play_time_clocktime",
"play_time_quarter",
"drive_num",
"drive_field_end_offset",
"drive_field_start_offset",
"drive_first_downs",
"drive_penalty_yds",
"drive_play_cnt",
"drive_pos_time_seconds",
"drive_result",
"drive_time_start",
"drive_time_end",
"drive_total_yds",
"game_away",
"game_home",
"game_eid",
"game_loser",
"game_winner",
"game_nice_score",
"game_score_home",
"game_score_away",
"game_score_home_q1",
"game_score_away_q1",
"game_score_home_q2",
"game_score_away_q2",
"game_score_home_q3",
"game_score_away_q3",
"game_score_home_q4",
"game_score_away_q4",
"game_score_home_q5",
"game_score_away_q5",
"game_week",
"game_year",
"game_time",
"game_wday",
"game_day",
"game_month",
"game_season_type",
"game_location",
"game_custom_id"
]

#build a collection of all the games in the API, due to library limitations, game collections will only represent games within a year, so we need a list of game collections
games_list = []
for year in range(2016,2017):
  games_list.append(nflgame.games(year))

#build a similar list of all plays available, due to the same limitation it must be a list of play collections
plays_list = []
for games in games_list:
  plays_list.append(nflgame.combine_plays(games))

#open the csv file, and build the header list from the sets of keys
with open('/mnt/c/Users/William/nfl_pbp_2016.csv', 'wb') as output:
  fieldnames = event_keys + ["play_" + i for i in play_keys] + ["player_" + i for i in player_keys] + other_keys
  csv_out = csv.DictWriter(output, fieldnames = fieldnames, delimiter = ',',quotechar = '"',restval = "", extrasaction = 'ignore')
  csv_out.writeheader()
  
  #loop through all the plays in the collection
  for plays in plays_list:
    for play in plays:
    
      #include every event that involves a player
      for event in play.events:
        if event["playerid"] is not None and len(event["playerid"]) > 3:
          out_dict = {}
          for key in event_keys:
            if key in event:
              out_dict[key] = event[key]
              
          #include the stats for the composite play that the event was part of
          for key in play_keys:
            if key in play._stats:
              out_dict["play_" + key] = play._stats[key]
            elif key in play.__dict__:
              out_dict["play_" + key] = play.__dict__[key]
              
          #lookup the player and include the player's data
          player = nflgame.players.get(event['playerid'])
          if player is not None:
            for key in player_keys:
              if key in player.__dict__:
                out_dict["player_" + key] = player.__dict__[key]
                
          #include misc play, drive, and game fields
          if play.yardline is not None:
            out_dict["play_yardline"] = play.yardline.offset
          if play.time is not None:
            out_dict["play_time_clocktime"] = play.time.clock
            out_dict["play_time_quarter"] = play.time.quarter
          if play.drive is not None:
            out_dict["drive_num"] = play.drive.drive_num
            if play.drive.field_end is not None:
              out_dict["drive_field_end_offset"] = play.drive.field_end.offset
            if play.drive.field_start is not None:
              out_dict["drive_field_start_offset"] = play.drive.field_start.offset
            out_dict["drive_first_downs"] = play.drive.first_downs
            out_dict["drive_penalty_yds"] = play.drive.penalty_yds
            out_dict["drive_play_cnt"] = play.drive.play_cnt
            if play.drive.pos_time is not None:
              out_dict["drive_pos_time_seconds"] = play.drive.pos_time.total_seconds()
            out_dict["drive_result"] = play.drive.result
            if play.drive.time_start is not None:
              out_dict["drive_time_start"] = play.drive.time_start.clock
            if play.drive.time_end is not None:
              out_dict["drive_time_end"] = play.drive.time_end.clock
            out_dict["drive_total_yds"] = play.drive.total_yds
            if play.drive.game is not None:
              out_dict["game_away"] = play.drive.game.away
              out_dict["game_home"] = play.drive.game.home
              out_dict["game_eid"] = play.drive.game.eid
              out_dict["game_loser"] = play.drive.game.loser
              out_dict["game_winner"] = play.drive.game.winner
              out_dict["game_nice_score"] = play.drive.game.nice_score()
              out_dict["game_score_home"] = play.drive.game.score_home
              out_dict["game_score_away"] = play.drive.game.score_away
              out_dict["game_score_home_q1"] = play.drive.game.score_home_q1
              out_dict["game_score_away_q1"] = play.drive.game.score_away_q1
              out_dict["game_score_home_q2"] = play.drive.game.score_home_q2
              out_dict["game_score_away_q2"] = play.drive.game.score_away_q2
              out_dict["game_score_home_q3"] = play.drive.game.score_home_q3
              out_dict["game_score_away_q3"] = play.drive.game.score_away_q3
              out_dict["game_score_home_q4"] = play.drive.game.score_home_q4
              out_dict["game_score_away_q4"] = play.drive.game.score_away_q4
              out_dict["game_score_home_q5"] = play.drive.game.score_home_q5
              out_dict["game_score_away_q5"] = play.drive.game.score_away_q5
              out_dict["game_week"] = play.drive.game.schedule['week']
              out_dict["game_year"] = play.drive.game.schedule['year']
              out_dict["game_time"] = play.drive.game.schedule['time']
              out_dict["game_wday"] = play.drive.game.schedule['wday']
              out_dict["game_day"] = play.drive.game.schedule['day']
              out_dict["game_month"] = play.drive.game.schedule['month']
              out_dict["game_season_type"] = play.drive.game.schedule['season_type']
              out_dict["game_location"] = play.drive.game.schedule['home']
              out_dict["game_custom_id"] = str(play.drive.game.schedule['year'] % 1000) + ("%02d" % play.drive.game.schedule['week']) + play.drive.game.home + play.drive.game.away
          
          csv_out.writerow(out_dict)
